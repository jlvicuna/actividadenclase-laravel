
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script> -->
    @if ($errors->any())
    <div class="alert alert-danger mt-2">
        <strong>Este es un mensaje de error...</strong> Algo fue mal..<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div>
      <a href="{{route('proyecto.index')}}" class="btn btn-primary">Volver</a>
    </div>
    <form action="{{route('proyecto.store')}}" method="post">
    	@csrf
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
              <div class="form-group">
                  <strong>Nombre proyecto:</strong>
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre" >
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
              <div class="form-group">
                  <strong>Descripción:</strong>
                  <textarea class="form-control" style="height:150px" name="descripcion" placeholder="Descripción..."></textarea>
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-2">
              <button type="submit" class="btn btn-primary">Crear</button>
          </div>
      </div>
    </form>
  </body>
</html>