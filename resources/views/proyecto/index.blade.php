

<html>
	<head>
		<style type="text/css">
			body{
				font-family: sans-serif;
				margin: 2em;
                padding: 2em;
                background-color: #7c8994;
			}
			.container{
				width: 80%;
                margin: 0 auto;
                background: white;
                padding: 20px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
			}
			h1 {
                color: #333;
            }

            h2 {
                color: #666;
/*                border-bottom: #3333b8b0;*/
            }

            li {
                margin-bottom: 10px;
            }

            /*.container .proyectos{
            	background-color: #3333b8b1;
            	border-bottom: #3333b8b0;
            	border-bottom: #0a0a21;
            	padding: 0.5em 0;
            	margin-bottom: 1.5em;
            	border-color: #3333b8b0;
            }*/

            /*ESTILOS PARA LA TABLA*/
            table {
                width: 100%;
                border-collapse: collapse;
            }

            th, td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

            tr:hover {background-color: #f5f5f5;}

            th {
                background-color: #f4f4f4;
                color: black;
            }

		</style>

	</head>
	<body>
		<div class="container">
			<h1>Hola, los siguientes son mis proyectos</h1>
            <table>
                @csrf
                <tr>
                    <th>Nombre del Proyecto</th>
                    <th>Descripción</th>
                </tr>
                @foreach ($proyectos as $proyecto)
                    <tr>
                        <td>{{ $proyecto->nombre }}
                            <a href="{{ route('proyecto.edit', $proyecto->id) }}"> Editar</a>
                            <form action="{{route('proyecto.destroy', $proyecto->id)}}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('¿Estás seguro de que quieres eliminar esta proyecto?')">Eliminar</button>
                            </form>
                         
                        </td>
                        
                        <td>{{ $proyecto->descripcion }}</td>
                        
                    </tr>
                @endforeach
                
            </table>
            <div>
                <a href="{{ route('proyecto.create') }}" class="btn btn-primary">Crear proyecto</a>
            </div>
            <!-- <button>Create</button> -->
            <!-- <button><a href="proyecto/create.blade.php"><span class="btonClick">Create</span></a></button> -->
		</div>
	</body>
</html>