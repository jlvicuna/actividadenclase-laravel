<style>
    :root {
        --background-color: #7c8994;
        --text-color: #333;
        --container-bg: #fff;
        --container-shadow: rgba(0, 0, 0, 0.1);
        --form-bg: blanchedalmond;
        --border-radius: 10px;
        --form-spacing: 20px;
    }

    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
        background-color: var(--background-color);
        color: var(--text-color);
    }

    .container {
        width: 80%;
        margin: 2em auto;
        background: var(--container-bg);
        padding: var(--form-spacing);
        box-shadow: 0 0 10px var(--container-shadow);
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    .form-group {
        margin-bottom: var(--form-spacing);
    }

    .form-group label {
        display: block;
        margin-bottom: 0.5em;
    }

    .form-control {
        display: block;
        width: 100%;
        padding: 0.5em;
        border-radius: var(--border-radius);
        background-color: var(--form-bg);
        border: 1px solid #ccc;
        margin-bottom: 1em;
    }

    .btn {
        padding: 0.5em 1em;
        background-color: #007bff;
        color: white;
        border: none;
        border-radius: var(--border-radius);
        cursor: pointer;
    }

    .btn.primary {
        background-color: #0056b3;
    }

    .btn + .btn {
        margin-left: 0.5em;
    }

    .btn:hover {
        background-color: #0069d9;
    }

    a {
        color: var(--text-color);
        text-decoration: none;
        padding: 0.5em 1em;
        display: inline-block;
    }

    a:hover {
        text-decoration: underline;
    }
</style>

<div class="container">
    <form action="{{ route('proyecto.update', $proyecto->id) }}" method="post">
        @csrf
        @method("put")
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $proyecto->nombre }}" required>
        </div>
        <div class="form-group">
            <label for="descripcion">Descripcion</label>
            <textarea class="form-control" id="descripcion" name="descripcion" rows="3" required>{{ $proyecto->descripcion }}</textarea>
        </div>
        <button type="submit" class="btn primary">Actualizar</button>
        <a href="{{ route('proyecto.index') }}" class="btn">Volver</a>
    </form>
</div>
