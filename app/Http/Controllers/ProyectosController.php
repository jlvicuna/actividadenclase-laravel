<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use App\Models\Proyectos;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //agregado en clase//
        $proyectos = DB::table("proyectos")->get();
        // $descripcionProyectos = DB::table("proyectos.descripcion")->get();
        return view("proyecto.index",["proyectos"=>$proyectos]);
    }
    // ,["descripcionProyectos"=>$descripcionProyectos]
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response    
     */
    public function create(): View
    {
        //agregado el 08/02 en clase
        return view("proyecto.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        //agregado el 08/02 en clase
        $request->validate([
            'nombre' => 'required|max:255',
            'descripcion' => 'required',
        ]);
        // dd($request->all());
        Proyectos::create($request->all());
        return redirect()->route('proyecto.index') ->with('success','Post created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proyectos  $proyectos
     * @return \Illuminate\Http\Response
     */
    public function show(Proyectos $proyectos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proyectos  $proyectos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // agregado el 14/02 con el código subido por la miss a las 9 pm
        $proyecto = Proyectos::find($id);
        return view('proyecto.edit', compact('proyecto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proyectos  $proyectos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyectos  $proyecto)
    {
        // agregado el 14/02 con el código subido por la miss a las 9 pm
        $request->validate([
                'nombre' => 'required|max:255',
                'descripcion' => 'required',
            ]);
        // $proyecto = Proyectos::find($id);
        $proyecto->update($request->all());
        return redirect()->route('proyecto.index')
        ->with('success','El proyecto se ha actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proyectos  $proyectos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // agregado el 14/02 con el código subido por la miss a las 9 pm
        $proyecto = Proyectos::find($id);
        $proyecto->delete();
        return redirect()->route('proyecto.index')
            ->with('success', 'el proyecto ha sido eliminado correctamente');
    }
}
