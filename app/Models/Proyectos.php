<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyectos extends Model
{
    use HasFactory;
    // agregado el 15/02 en clase
    protected $fillable = ['nombre', 'descripcion'];
}
